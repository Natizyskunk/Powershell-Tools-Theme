<# Changig execution policy to enable Powershell to run RemoteSigned scripts #>
Set-ExecutionPolicy RemoteSigned -scope Process -Force -Confirm:$false
Set-ExecutionPolicy RemoteSigned -scope CurrentUser -Force -Confirm:$false

<# Removing Old Installation to make new clean Install. #>
Stop-Process -name ssh-agent -Force -Confirm:$false
remove-item -path "C:/Tools/" -Force -Recurse -ErrorAction SilentlyContinue
remove-item -path "$ENV:UserProfile/scoop/" -Force -Recurse -ErrorAction SilentlyContinue
remove-item -path "$ENV:UserProfile/pshazz/" -Force -Recurse -ErrorAction SilentlyContinue
remove-item -path "$ENV:UserProfile/Documents/WindowsPowerShell/" -Force -Recurse -ErrorAction SilentlyContinue
remove-item -path "$ENV:UserProfile/AppData/Local/Temp/" -Force -Recurse -ErrorAction SilentlyContinue
remove-item -path "$ENV:UserProfile/.ssh/agent.env.ps1" -Force -Recurse -ErrorAction SilentlyContinue
remove-item -path "$ENV:UserProfile/.gitconfig" -Force -Recurse -ErrorAction SilentlyContinue
remove-item -path "$ENV:UserProfile/.pshazz" -Force -Recurse -ErrorAction SilentlyContinue
remove-item -path "$ENV:UserProfile/.scoop" -Force -Recurse -ErrorAction SilentlyContinue
remove-item -path "$ENV:UserProfile/navdb.csv" -Force -Recurse -ErrorAction SilentlyContinue

<# Downloading & Installing Scoop #>
iex (new-object net.webclient).downloadstring('https://get.scoop.sh')
scoop update

<# Changig execution policy to enable Powershell to Force Bypass Process signature verification #>
Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass -Force -Confirm:$false

<# Downloading & Installing Chocolatey #>
iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco upgrade chocolatey -Force -Confirm:$false

<# Downloading & Installing posh-git #>
choco install poshgit -Force -Confirm:$false
choco upgrade poshgit -Force -Confirm:$false

<# Downloading & Installing Powershell Tools #>
scoop uninstall 7zip git openssh concfg pshazz
scoop install 7zip git openssh concfg pshazz

<# back-up current console settings #>
concfg export "PS-Themes\console-backup.json"

<# Downloading & Installing Powershell Theme #>
mkdir "$ENV:UserProfile/pshazz/"
Copy-Item "Pshazz-Themes\colorfull.json" -Destination "$ENV:UserProfile/pshazz/"
Copy-Item "Pshazz-Themes\colorfull2.json" -Destination "$ENV:UserProfile/pshazz/"
pshazz use colorfull2
concfg import "PS-Themes\monokai.json"