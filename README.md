# Powershell Tools + Theme.

**Included:** Scoop, Chocolatey, Poshgit, 7zip, Git, Openssh, Concfg, Pshazz.

![](/Preview.png "Preview")

## How-To.
**1.** Clone the repository with Git or donwload or download latest release. <BR />
**2.** Run Powershell as Administrator (It's really important otherwise the installation will be aborted!). <BR />
**3.** Enter: `Set-ExecutionPolicy RemoteSigned -scope Process -Force -Confirm:$false`<BR /> ... Also: `Set-ExecutionPolicy RemoteSigned -scope CurrentUser -Force -Confirm:$false`<BR /> ... And: `Set-ExecutionPolicy RemoteSigned -scope CurrentUser` <BR />
**4.** cd to where Powershell-Tweaks.ps1 is located (in the folder you've cloned in step 1). <BR />
**5.** Enter: `powershell.exe -File Powershell-Tweaks.ps1` <BR />
**6.** Let the script do it work and wait for it to be finished. <BR />
**7.** VOILÀ !

## Sources.
- Scoop.sh: http://scoop.sh
- Chocolatey: https://chocolatey.org
- Concfg: https://github.com/lukesampson/concfg
- Theming-Powershell: https://github.com/lukesampson/scoop/wiki/Theming-Powershell
